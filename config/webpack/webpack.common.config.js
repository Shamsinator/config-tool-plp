const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

// webpack rules
const rules = require('./rules');

module.exports = {
    entry: './src/index.tsx',
    output: {
        filename: '[name].js',
        chunkFilename: 'chunk-[id].js',
        path: path.resolve(__dirname, '../../dist'),
        publicPath: '/',
    },
    module: { rules: rules() },

    resolve: {
        alias: {
            '@scss': path.resolve(__dirname, '../../src/assets/scss/'),
            '@': path.resolve(__dirname, '../../src/'),
        },
        modules: [
            'node_modules',
            path.resolve(__dirname, 'src'),
        ],
        extensions: ['.js', '.tsx', '.ts', '.scss', '.json'],
    },

    plugins: [
        new MiniCssExtractPlugin({
            chunkFilename: '[id].css',
            filename: '[name].css',
        }),
        new HtmlWebpackPlugin({
            template: './src/index.html',
            inject: true,
            minify: {
                removeComments: false,
                collapseWhitespace: false,
            },
        }),
        new CleanWebpackPlugin(),
    ],
};
