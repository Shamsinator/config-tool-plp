const { merge } = require('webpack-merge');
const webpack = require('webpack');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const webpackBaseConfig = require('./webpack.common.config.js');

module.exports = merge(webpackBaseConfig, {
    devServer: {
        historyApiFallback: true,
        port: process.env.WEBPACK_DEV_SERVER_PORT,
        disableHostCheck: true,
        hot: true,
        watchOptions: {
            aggregateTimeout: 300, // delay before reloading
            poll: 1000, // enable polling since fsevents are not supported in docker
        },
    },
    devtool: 'eval-source-map',
    mode: 'development',
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new BundleAnalyzerPlugin({
            analyzerMode: 'static',
            reportFilename: 'BundleAnalyzer.html',
            openAnalyzer: false,
            statsOptions: { source: false },
        }),
        new webpack.DefinePlugin({
            'process.env': JSON.stringify(process.env),
        }),
    ],
});
