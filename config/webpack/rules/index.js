const javascript = require('./javascript');
const css = require('./css');

module.exports = () => (
    [
        javascript(),
        css.sassModules(),
        css.sass(),
    ]
);
