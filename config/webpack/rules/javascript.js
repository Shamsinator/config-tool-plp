module.exports = () => ({
    test: /\.(js|ts|tsx)$/,
    exclude: /node_modules\/(?!react-intl)/,
    use: {
        loader: 'babel-loader',
    },
});
