const MiniCssExtractPlugin = require('mini-css-extract-plugin');

exports.sassModules = () => (
    {
        test: /\.scss$/,
        use: [
            { loader: MiniCssExtractPlugin.loader },
            {
                loader: 'css-loader',
                options: {
                    modules: {
                        localIdentName: '[local]--[hash:base64:5]',
                    },
                    importLoaders: 1,

                },
            },
            { loader: 'postcss-loader' },
            { loader: 'sass-loader' },
            {
                loader: 'sass-resources-loader',
            },
        ],
        include: /\.module\.scss$/,
    }
);

exports.sass = () => (
    {
        test: /\.scss$/,
        use: [
            { loader: MiniCssExtractPlugin.loader },
            { loader: 'css-loader' },
            { loader: 'postcss-loader' },
            { loader: 'sass-loader' },
            {
                loader: 'sass-resources-loader',
            },
        ],
        exclude: /\.module\.scss$/,
    }
);
